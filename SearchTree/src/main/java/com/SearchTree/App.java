package com.SearchTree;

public class App 
{
    public static void main( String[] args )
    {
        //System.out.println( "Hello World!" );
        BinarySearchTree tree = new BinarySearchTree();
        tree.insert(3, 1);
        tree.insert(2, 2);
        tree.insert(4, 5);
        tree.print();
        tree.remove(2);
        tree.remove(3);
        tree.print();
        tree.remove(4);
    }
}
